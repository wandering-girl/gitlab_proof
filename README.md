# Fingerprint Verification

[Verifying my OpenPGP key: openpgp4fpr:FC1552A1B7C38D189AE80FEB7DBC94EA91424CFF]

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://docs.keyoxide.org/advanced/openpgp-proofs/
